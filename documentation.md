# Documentation du projet de Test Leboncoin

  ## Installation
  
1. Lancer la commande php composer.phar install pour installer les modules/bundles qui se trouvent dans composer.json et systématiquement la commande va créer composer.lock et le dossier /vendor  
2. Créer une BDD 'crm' avec le host mysql 'localhost', le user 'root' et la mot de passe '' (Voir config.php)  
3. Lancer le scripte qui se trouve dans "upgarde.sql" sur notre BDD pour la création et l'amélioration. 
  
  ## Travail réalisé
1. Ajouter le code manquant et corriger les éventuels bugs  
2. Mettre en place un routeur (gestion des redirections)   
3. Développer les écrans :  
    *  Écran de login  
    *  Écran de liste des contacts
    *  Écran d’édition de contact
    *  Écran de création du contact
    *  Écran d’édition d'adresse
    *  Écran de création d’adresses
4. Écrire la documentation d’installation   
5. Optimiser la base de données

## Travail à améliorer
1. Faire les tests unitaires et fonctionnels en utilisant PhpUnit et BEHAT/Mink/Selenium
2. Le fonctionnement de routeur (Dans le projet j'ai créé un simple routeur pour faire la redirection d'un url vers les fonctions qui se trouvent dans le dossier /controllers.) 
*Exemple :* URL/Nom_controller/Action/ID
3. Ajouter un mode "DEV" et mode "PROD" pour faire la redirection des exceptions et les erreurs vers une page "erreur" en prod ou afficher les messages d'erreur en mode dev pour le débogage.
4. Hachage du mot de passe  : Lors de l'inscription,  sha1  ,  md5  ,  sha256  ,  sha512  ... ne sont plus considérées comme des fonctions de hachage sûres aujourd'hui.  Il faut pas les utilisez. mais il faut utiliser password_hash qui choisit le meilleur algorithme pour nous.  
*Exemple :*  password_hash($_POST['pass'], PASSWORD_DEFAULT);
5. Afichage de messages d'erreur et de success
6. Ajouter des panels de confirmation de suppression.
7. Mettre les urls plus dynamique dans le code de projet.
8. Utiliser doctrine comme ORM pour le mapping de la BDD.