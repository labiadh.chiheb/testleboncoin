<?php

namespace App\Model;

use App\Models\AbstractModel;

class UserModel extends AbstractModel
{
    /** @var string  */
    protected $table = "users";

    /**
     * Retourner un utilisateur par login
     *
     * @param $login
     *  @return array|bool|mixed|\PDOStatement
     */
    public function getUserByLogin(string $login)
    {
        return $this->query("SELECT * FROM {$this->table} WHERE login = $login");
    }
}