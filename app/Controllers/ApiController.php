<?php

namespace App\Controllers;

class ApiController extends MainController
{
    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function checkEmail()
    {
        return $this->api->email();
    }

    /**
     * @throws \Exception
     */
    public function checkPalindrome()
    {
        return $this->api->palindrome();
    }
}