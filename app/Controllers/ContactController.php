<?php

namespace App\Controllers;

use App\Components\Api;
use App\Models\ContactModel as ContactModel;
use App\Controllers\ControllerInterface;
use InvalidArgumentException;
use Exception;

class ContactController extends MainController implements ControllerInterface
{
    /** @var int $userId */
    protected $userId;

    /**
     * @var ContactModel
     */
    protected $modelContact;

    /**
     * ContactController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->modelContact = $this->loadModel('Contact');

        if ($this->auth->logged()) {
            $this->userId = $_SESSION['auth']['id'];
        }
    }

    /**
     * @return int
     */
    protected function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * Affichage de la liste des contacts de l'utilisateur connecté
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function index()
    {
        $contacts = [];
        if (!empty($this->getUserId())) {
            $contacts = $this->modelContact->getContactByUser($this->getUserId());
        } else {
            $this->redirect("User/login");
        }

        echo $this->twig->render('contact/index.html.twig', ['contacts' => $contacts]);
    }

    /**
     * Ajout d'un contact
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws Exception
     */
    public function add()
    {
        $error = false;
        if (!empty($_POST)) {
            $response = $this->sanitize($_POST);
            if ($response["response"]) {
                $result = $this->modelContact->create([
                    'nom'    => $response['nom'],
                    'prenom' => $response['prenom'],
                    'email'  => $response['email'],
                    'userId' => $this->userId
                ]);
                if ($result) {
                    $this->redirect("Contact/index");
                } else {
                    $error = true;
                }
            } else {
                $error = true;
            }
        }

        echo $this->twig->render('contact/add.html.twig', ['error' => $error]);
    }

    /**
     * Modification d'un contact
     *
     * @param int $idContact
     * @return string
     * @throws Exception
     */
    public function edit(int $idContact)
    {
        //Trouver le contact par id à modifier
        $oContact = $this->modelContact->findById($idContact);
        if (empty($oContact)) {
            throw new Exception('Id de contact n\'exisite pas');
        }

        $error = false;
        if (!empty($_POST)) {
            $response = $this->sanitize($_POST);
            if ($response["response"]) {
                $result = $this->modelContact->update($idContact, [
                    'nom'    => $response['nom'],
                    'prenom' => $response['prenom'],
                    'email'  => $response['email'],
                ]);
                if ($result) {
                    $this->redirect("Contact/index");
                }
            } else {
                $error = true;
            }
        }

        echo $this->twig->render('contact/edit.html.twig', [
            'error' => $error,
            'contact' => $oContact
        ]);
    }

    /**
     * Suppression d'un contact
     * @param int $iIdContact
     * @throws Exception
     */
    public function delete(int $iIdContact)
    {
        $result = $this->modelContact->delete($iIdContact);
        if ($result) {
            $this->redirect("Contact/index");
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function sanitize(array $data = []): array
    {
        //fonction pour securiser les donnees à enregistree
        $data = $this->secure_form($data);

        $prenom = strtoupper($data['prenom']);
        $nom    = strtoupper($data['nom']);
        $email  = strtolower($data['email']);

        if (empty($prenom)) {
            throw new Exception('Le prenom est obligatoire');
        }

        if (empty($nom)) {
            throw new Exception('Le nom est obligatoire');
        }

        //Pour améliorer le temps d'exécution de l'application je préfére de faire le teste directement ici
        //$isPalindrome = $this->apiClient('Api/checkPalindrome', ['name' => $nom]);
        if ($nom === strrev($nom)) {
            throw new Exception('Le nom du contact ne peut pas être un palindrome');
        }

        if (empty($email)) {
            throw new Exception('Le email est obligatoire');
        } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Le format de l\'email est invalide');
        }

        $isEmail = $this->apiClient('Api/checkEmail', ['email' => $email]);
        if (!isset($isEmail->response) || !$isEmail->response) {
            throw new Exception('L\'adresse mail n\'a pas la bonne format');
        }

        return [
            'response' => true,
            'email'    => $email,
            'prenom'   => $prenom,
            'nom'      => $nom
        ];
    }
}