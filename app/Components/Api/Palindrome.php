<?php
/**
 * Created by PhpStorm.
 * User: chiheb
 * Date: 15/02/2019
 * Time: 14:04
 */

namespace App\Components\Api;

use Exception;

class Palindrome
{
    /** @var String */
    protected $name;

    /**
     * palindrome constructor.
     */
    public function __construct() {
    }

    /**
     * @return String
     */
    public function getName(): String {
        return $this->name;
    }

    /**
     * @param String $name
     */
    public function setName(String $name) {
        $this->name = $name;
    }

    /**
     *
     * Vérifier la validité du nom
     *
     * @return bool
     * @throws Exception
     */
    public function is_valid(){
        if (is_null($this->getName())) {
            throw new Exception('Le nom ne peut pas être null');
        }
        if ($this->getName() === strrev($this->getName())) {
            return false;
        } else {
            return true;
        }
    }
}